import itertools
import numpy as np
import pandas as pd
import os
from os import path, listdir
import seaborn.apionly as sns
import matplotlib.pyplot as plt

# Defining style
plt.style.use('style.conf')
custom = ["#4070C0", "#F34444", "#EEC400", "#D28A20", "#34495e", "#2ecc71"]

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'perfusion.csv'))

days = df['Days Post Transsection'].unique()
treatments = df['Treatment'].unique()
sections = ['Proximal Flow Time [s]','Wound Flow Time [s]','Distal Flow Time [s]']

# Calculate percent perfused
df_ = []
for treatment, day in itertools.product(treatments, days):
	row = {'Treatment':treatment,'Days Post Transsection':day}
	for section in sections:
		my_slice = df.loc[(df['Treatment']==treatment) & (df['Days Post Transsection']==day), section]
		my_slice = my_slice.astype(str)
		try:
			perfused_ = my_slice.value_counts()['True']
		except KeyError:
			perfused_ = 0
		try:
			not_perfused = my_slice.value_counts()['False']
		except KeyError:
			not_perfused = 0
		my_slice = my_slice.str.extract('(\d+)', expand=False)
		perfused = my_slice.notna().sum()
		row[section] = (perfused+perfused_) / float(not_perfused+perfused+perfused_) * 100
	df_.append(row)
df_ = pd.DataFrame(df_)
df_ = df_.rename(index=str, columns={'Proximal Flow Time [s]':'Proximal Arteries Perfused [\%]','Wound Flow Time [s]':'Wound Perfused [\%]','Distal Flow Time [s]':'Distal Arteries Perfused [\%]'})

df_ = df_.melt(id_vars=['subject','Treatment','Days Post Transsection'],value_vars=['Wound Perfused [\%]','Proximal Arteries Perfused [\%]','Distal Arteries Perfused [\%]'],var_name='Region',value_name='Perfused [\%]')
df_['Region'] = df_['Region'].map({
	'Proximal Arteries Perfused [\%]':'Proximal Arteries',
	'Wound Perfused [\%]':'Wound',
	'Distal Arteries Perfused [\%]':'Distal Arteries',
	})
with plt.style.context('116.conf'):
	fig, ax = plt.subplots()
	sns.catplot(
		x='Days Post Transsection',
		y='Perfused [\%]',
		hue='Treatment',
		col='Region',
		data=df_,
		kind='bar',
		palette=custom,
		hue_order=['Control','AZD4547','Sunitinib','AZD+Sun.'],
		col_order=[
			'Proximal Arteries',
			'Wound',
			'Distal Arteries',
			],
		aspect=1.1,
		legend_out=False,
		dodge=.15,
		)
	plt.savefig('perfusionpercent_catplot.pdf')
