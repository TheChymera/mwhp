# -*- coding: utf8 -*-

import pandas as pd
import os
from os import path, listdir
import seaborn.apionly as sns
import matplotlib.pyplot as plt
from scipy import stats

# Defining style
plt.style.use('style.conf')
custom = ["#61B329", "#8E388E"]

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'calcium.csv'))

day_palette = sns.cubehelix_palette(len(df['Days Post Transsection'].unique()),light=.75,dark=.25,rot=-1,start=.8)

with plt.style.context('147.conf'):
	fig, ax1 = plt.subplots()
	ax = sns.catplot(
		x='Days Post Transsection',
		y='Spikes',
		col='Artery',
		data=df,
		kind='swarm',
		aspect=1.2,
		palette=day_palette,
		legend_out=False,
		)
	ax.axes[0,0].set_ylabel('Calcium Spikes in 22\' Period')
	plt.savefig('calcium_catplot.pdf')

df[['Days Post Transsection','Spikes']] = df[['Days Post Transsection','Spikes']].apply(pd.to_numeric)
fig, ax1 = plt.subplots()
ax = sns.tsplot(
	time='Days Post Transsection',
	value='Spikes',
	condition='Artery',
	unit='subject',
	data=df,
	err_style="unit_traces",
	color=custom,
	marker='',
	interpolate=False,
	)
handles = []
labels = []
for ix, i in enumerate(df['Artery'].unique()):
	label = i
	handle, = ax.plot([], label=label, color=custom[ix], alpha=0.5)
	handles.append(handle)
	labels.append(label)
ax.legend(handles,labels)
ax.set_xlim(min(df['Days Post Transsection'].unique()),max(df['Days Post Transsection'].unique()))
ax.set_ylabel('Calcium Spikes in 22\' Period')
plt.savefig('calcium_tsplot.pdf')

df = df.loc[df['Artery']=='Distal']
df_length = pd.read_csv(path.join(data_dir,'sprout-length.csv'))
df_length = df_length.loc[df_length['Artery']=='Distal']
df_sprout = pd.read_csv(path.join(data_dir,'sprout-count.csv'))
df_sprout = df_sprout.loc[df_sprout['Artery']=='Distal']
df_sprout['ID'] = df_sprout['subject'] + df_sprout['Days Post Transsection'].map(str)
df_length['ID'] = df_length['subject'] + df_length['Days Post Transsection'].map(str)
df['ID'] = df['subject'] + df['Days Post Transsection'].map(str)

df_length = df_length[['ID','Average Sprout Length [µm]']]
df_ = pd.merge(df_length, df_sprout, on=['ID'])
df_['Sprout Reach'] = df_['Sprouts'] * df_['Average Sprout Length [µm]']

df = df[['ID','Spikes']]
df__ = pd.merge(df, df_, on=['ID'])
fig, ax1 = plt.subplots()
ax = sns.jointplot(
	x="Sprouts",
	y="Spikes",
	data=df__,
	kind="reg",
	color="k",
	)
ax.annotate(stats.spearmanr)
from matplotlib.ticker import MaxNLocator
plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
plt.savefig('sprout-calcium_correlation.pdf')

fig, ax1 = plt.subplots()
ax = sns.jointplot(
	x="Sprout Reach",
	y="Spikes",
	data=df__,
	kind="reg",
	color="k",
	)
ax.annotate(stats.spearmanr)
from matplotlib.ticker import MaxNLocator
plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
plt.savefig('sprout_reach-calcium_correlation.pdf')
