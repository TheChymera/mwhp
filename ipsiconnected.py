import itertools
import pandas as pd
import os
from os import path, listdir
import seaborn.apionly as sns
import matplotlib.pyplot as plt

# Defining style
plt.style.use('style.conf')
custom = ["#4070C0", "#F34444", "#EEC400", "#D28A20", "#34495e", "#2ecc71"]

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'ipsiconnected.csv'))

df_ = df.loc[(df['Treatment']=='Control')]
fig, ax1 = plt.subplots()
ax = sns.catplot(
	x='Days Post Transsection',
	y='Artery-to-Vein Connected [\%]',
	col='Artery',
	hue='Treatment',
	data=df_,
	kind='bar',
	aspect=.5,
	palette=custom,
	legend_out=False,
	legend=None,
	)
plt.savefig('ipsiconnected_control_catplot.pdf')

with plt.style.context('088.conf'):
	df_ = df.loc[df['Artery']=='Distal']
	fig, ax = plt.subplots()
	sns.barplot(
		x='Days Post Transsection',
		y='Artery-to-Vein Connected [\%]',
		hue='Treatment',
		data=df_,
		palette=custom,
		dodge=.15,
		hue_order=['Control','AZD4547','Sunitinib','AZD+Sun.'],
		)
	ax.set_ylabel('Distal Artery-to-Vein Connected [\%]')
	ax.set_title('Artery = Distal')
	plt.savefig('ipsiconnected_distal.pdf')

	df_ = df.loc[df['Artery']=='Proximal']
	fig, ax = plt.subplots()
	sns.barplot(
		x='Days Post Transsection',
		y='Artery-to-Vein Connected [\%]',
		hue='Treatment',
		data=df_,
		palette=custom,
		dodge=.15,
		hue_order=['Control','AZD4547','Sunitinib','AZD+Sun.'],
		)
	ax.set_ylabel('Proximal Artery-to-Vein Connected [\%]')
	ax.set_title('Artery = Proximal')
	plt.savefig('ipsiconnected_proximal.pdf')

fig, ax1 = plt.subplots()
ax = sns.catplot(
	x='Days Post Transsection',
	y='Artery-to-Vein Connected [\%]',
	col='Artery',
	hue='Treatment',
	data=df,
	kind='bar',
	aspect=.85,
	palette=custom,
	legend_out=False,
	)
plt.savefig('ipsiconnected_catplot.pdf')

