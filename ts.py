import pandas as pd
import os
from os import path, listdir
import seaborn.apionly as sns
import matplotlib.pyplot as plt
import numpy as np

# Defining style
plt.style.use('style.conf')
plt.style.use('ts.conf')
custom = ["#4070C0", "#F34444", "#D9C400", "#DD8B20", "#34495e", "#2ecc71"]

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'ts.csv'))
df = df.replace('True', '', regex=True)

df['Intensity [a.U.]'] = df['Intensity [a.U.]']/10000

sns.lineplot(x="Time [s]",
	y="Intensity [a.U.]",
	data=df,
	color=custom[1],
	sizes=[10],
	)
plt.xlim(0,68)
import matplotlib.ticker as plticker
loc = plticker.MultipleLocator(base=2) # this locator puts ticks at regular intervals
plt.gca().xaxis.set_major_locator(loc)
plt.savefig('ts.pdf')
