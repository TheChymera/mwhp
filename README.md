# Top-Level Data and Analysis Scripts for the “An in Vivo Wound Healing Model for the Characterization of the Angiogenic Process and its Modulation by Pharmacological Interventions” Paper
This repository exists to transparently document the statistical methods of the paper and ship them in a reexeutable format.
To give academic credit please cite exclusively the “An in Vivo Wound Healing Model for the Characterization of the Angiogenic Process and its Modulation by Pharmacological Interventions” Paper, as it already references this repository, where needed.

## Install
The files reposited here are best stored and run in userspace, therefore installation is as easy as:

```
git clone git@bitbucket.org:TheChymera/mwhp.git
```

Additionally, however, the package has a number of [external dependencies](#dependencies) which need to be resolved.

## Dependencies

### Automatic Install

Dependencies can be automatically installed by any package manager which respects the [Package Manager Soecification](https://dev.gentoo.org/~ulm/pms/head/pms.html).
This is done by running the install file as root with the following commands:

```
cd mwhp/.gentoo
./install.sh -oav
```

### Manual Install

The most precise description of the dependency graph (including conditionality) can be extracted from the [mwhp ebuild](.gentoo/sci-biology/mwhp/mwhp-99999.ebuild).
Alternatively you may refer to the following list:

* [matplotlib](https://matplotlib.org/)
* [numpy](https://www.numpy.org/)
* [pandas](https://pandas.pydata.org/)
* [seaborn](https://seaborn.pydata.org/) (>=`0.9.0`)
* [statsmodels](https://www.statsmodels.org/stable/index.html) (>`0.9.0`)
* [scipy](https://www.scipy.org/scipylib/index.html)
* [TeX Live](https://tug.org/texlive/)


## Run
All analysis scripts can be sequentially executed with the following command:

```
cd mwhp
./run.sh
```
