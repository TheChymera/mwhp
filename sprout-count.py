# -*- coding: utf8 -*-

import pandas as pd
import os
from os import path, listdir
import seaborn.apionly as sns
import matplotlib.pyplot as plt

# Defining style
plt.style.use('style.conf')
custom = ["#4070C0", "#F34444", "#D9C400", "#DD8B20", "#34495e", "#2ecc71"]
custom_ = ["#61B329", "#8E388E"]

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'sprout-count.csv'))

df_ = df.loc[df['Treatment']=='Control']
fig, ax1 = plt.subplots()
ax = sns.pointplot(
	x='Days Post Transsection',
	y='Sprouts',
	hue='Artery',
	data=df_,
	size=6,
	ci=90,
	dodge=.15,
	hue_order=sorted(df['Artery'].unique()),
	palette=custom_,
	)
ax.set_ylabel('Sprouts')
plt.savefig('sprout-count_control_side_pointplot.pdf')

df = df.loc[df['Artery']=='Distal']
fig, ax1 = plt.subplots()
ax = sns.pointplot(
	x='Days Post Transsection',
	y='Sprouts',
	hue='Treatment',
	data=df,
	size=6,
	ci=90,
	dodge=.15,
	hue_order=['Control','AZD4547','Sunitinib','AZD+Sun.'],
	palette=custom,
	)
ax.set_ylabel('Sprouts')
plt.savefig('sprout-count_pointplot.pdf')
