# -*- coding: utf8 -*-

import itertools
import numpy as np
import pandas as pd
import statsmodels.formula.api as smf
from statsmodels.stats.multitest import multipletests
from os import path

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'sprout-length.csv'))
df = df.dropna(axis=0,how='any')

model = smf.mixedlm('Q("Average Sprout Length [µm]") ~ Q("Days Post Transsection") : Treatment', df, groups='subject')
fit = model.fit()
summary = fit.summary()

levels = [
	'Q("Days Post Transsection"):Treatment[AZD+Sun.]',
	'Q("Days Post Transsection"):Treatment[AZD4547]',
	'Q("Days Post Transsection"):Treatment[Control]',
	'Q("Days Post Transsection"):Treatment[Sunitinib]',
	]
comparisons = [[a,b] for a, b in itertools.combinations(levels,2)]
contrasts = []
for comparison in comparisons:
	contrast = [int(i in comparison) for i in levels]
	# Add column for intercept:
	contrast = [0]+contrast
	second = False
	for ix, i in enumerate(contrast):
		if i==1 and second:
			break
		elif i == 1:
			second=True
	contrast[ix] = -1
	contrasts.append(contrast)

f_contrast = ['{} - {}'.format(a[0],a[1]) for a in comparisons]
f_contrast = ','.join(f_contrast)
f = fit.f_test(f_contrast)

t_tests = fit.t_test(np.array(contrasts))

legend=''
g, corrected_pvalues, _, _ = multipletests(t_tests.pvalue, alpha=0.05, method='holm')
for ix, p in enumerate(corrected_pvalues):
	legend += 'c{} = {}:\n\tHolm-Bonferroni corrected p={}\n'.format(ix,comparisons[ix],p)

with open("sprout-lengthANOVA.txt", "w") as text_file:
	text_file.seek(0)
	text_file.write(summary.as_text())
	text_file.write('\n')
	text_file.write(f.__str__())
	text_file.write('\n\n')
	text_file.write(legend)
	text_file.write('\n')
	text_file.write(t_tests.__str__())
	text_file.write('\n')
