# -*- coding: utf8 -*-

import numpy as np
import pandas as pd
import statsmodels.formula.api as smf
from os import path

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'sprout-count.csv'))
df = df.loc[df['Artery']=='Distal']
df = df.dropna(axis=0,how='any')

formula='Q("Perfused Sprouts") ~ Sprouts'
model = smf.mixedlm(formula, df, groups='subject')
fit = model.fit()
summary = fit.summary()
with open("sprout-perfusedGLM.txt", "w") as text_file:
    text_file.write(summary.as_text())

df = df.loc[df['Treatment']=='Control']
model = smf.mixedlm(formula, df, groups='subject')
fit = model.fit()
summary = fit.summary()
with open("sprout-perfused_controlGLM.txt", "w") as text_file:
    text_file.write(summary.as_text())
