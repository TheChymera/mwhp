# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python{2_7,3_4,3_5,3_6} )

DESCRIPTION="Analysis scripts for wound healing characterization."
HOMEPAGE="https://bitbucket.org/TheChymera/mwhp"

LICENSE="CC-BY-SA-3.0"
SLOT="0"
IUSE=""
KEYWORDS=""

DEPEND=""
RDEPEND="
	app-text/texlive
	dev-python/matplotlib
	dev-python/numpy
	dev-python/pandas
	>=dev-python/seaborn-0.9.0
	>dev-python/statsmodels-0.10.0
	sci-libs/scipy
	"

src_unpack() {
	cp -r -L "$DOTGENTOO_PACKAGE_ROOT" "$S"
}
