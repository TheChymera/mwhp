# -*- coding: utf8 -*-

import pandas as pd
import os
from os import path, listdir
from scipy import stats
import seaborn.apionly as sns
import matplotlib.pyplot as plt

# Defining style
plt.style.use('style.conf')
custom = ["#4070C0", "#F34444", "#D9C400", "#DD8B20", "#34495e", "#2ecc71"]
order = ['Control','AZD4547','Sunitinib','AZD+Sun.']

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'sprout-count.csv'))
df = df.loc[df['Artery']=='Distal']
df = df.dropna(subset=['Perfused Sprouts'])
df['Sprouts'] = df['Sprouts'].astype(int)

fig, ax1 = plt.subplots()
ax = sns.jointplot(
	x="Sprouts",
	y="Perfused Sprouts",
	data=df,
	kind="reg",
	color="k",
	)
handles=[]
for ix, treatment in enumerate(order):
	df_ = df.loc[df['Treatment']==treatment]
	h, = ax.ax_joint.plot(df_['Sprouts'], df_['Perfused Sprouts'], "o", ms=7, color=custom[ix], label=treatment)
	handles.append(h)
legend_ = plt.legend(handles=handles)
ax.annotate(stats.spearmanr)
plt.gca().add_artist(legend_)
from matplotlib.ticker import MaxNLocator
plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
plt.savefig('sprout-perfused_correlation.pdf')

with plt.style.context('105.conf'):
	fig, ax1 = plt.subplots()
	ax = sns.jointplot(
		x="Sprouts",
		y="Perfused Sprouts",
		data=df.loc[df['Treatment']=='Control'],
		kind="reg",
		color=custom[0],
		)
	ax.annotate(stats.spearmanr)
	from matplotlib.ticker import MaxNLocator
	plt.gca().yaxis.set_major_locator(MaxNLocator(integer=True))
	plt.savefig('sprout-perfused_correlation_control.pdf')

with plt.style.context('compact.conf'):
	fig, ax1 = plt.subplots()
	ax = sns.swarmplot(
		x="Treatment",
		y="Perfused Sprouts",
		hue="Treatment",
		data=df,
		hue_order=order,
		order=order,
		palette=custom,
		)
	plt.savefig('sprout-perfused_treatment.pdf')
