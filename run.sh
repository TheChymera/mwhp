#!/usr/bin/env bash

for i in *py; do
	python $i || \
		{ echo "Script $i failed to execute (see traceback above)."; exit 1; }
done
