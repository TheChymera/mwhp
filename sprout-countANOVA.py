# -*- coding: utf8 -*-

import itertools
import numpy as np
import pandas as pd
import scipy as sp
import statsmodels.api as sm
import statsmodels.formula.api as smf
from os import path
from statsmodels.genmod.bayes_mixed_glm import PoissonBayesMixedGLM
from statsmodels.stats.multitest import multipletests

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'sprout-count.csv'))
df = df.drop('Perfused Sprouts', 1)
df = df.dropna(axis=0,how='any')

formula="Sprouts ~ Treatment : I(np.log(Q('Days Post Transsection')))"
model = PoissonBayesMixedGLM.from_formula(formula, {'subject': '0 + C(subject)'}, df)
fit = model.fit_map()
summary = fit.summary()

means = fit.fe_mean[1:]
sds = fit.fe_sd[1:]
zvalues = means / sds
pvalues = 2*sp.stats.norm.cdf(-np.abs(zvalues))
levels = fit.model.names[1:len(pvalues)+1]

level_significance='Individual Level Significance:\n'
g, corrected_pvalues, _, _ = multipletests(pvalues, alpha=0.05, method='holm')
for ix, level in enumerate(levels):
	level_significance += '\t{} : p={}, Holm-Bonferroni corrected p={}\n'.format(level,pvalues[ix],corrected_pvalues[ix])

comparisons = [[a,b] for a, b in itertools.combinations(levels,2)]
contrasts = []
params = fit.params

for comparison in comparisons:
	a_pos = [i for i,x in enumerate(levels) if x == comparison[0]][0]
	b_pos = [i for i,x in enumerate(levels) if x == comparison[1]][0]

comparison_pvalues = []
for comparison in comparisons:
	contrast = [int(i in comparison) for i in levels]
	# Add column for intercept:
	contrast = [0]+contrast
	contrast += [0] * (len(params) - len(contrast))
	second = False
	for ix, i in enumerate(contrast):
		if i==1 and second:
			break
		elif i == 1:
			second=True
	contrast[ix] = -1
	mean = np.dot(contrast, fit.params)
	sd = np.sqrt(np.dot(contrast, np.dot(fit._cov_params, contrast)))
	zvalue = mean / sd
	pvalue = 2*sp.stats.norm.cdf(-np.abs(zvalue))
	comparison_pvalues.append(pvalue)

comparison_significance='Comparison Significance:\n'
g, corrected_comparison_pvalues, _, _ = multipletests(comparison_pvalues, alpha=0.05, method='holm')
for ix, p in enumerate(corrected_comparison_pvalues):
	comparison_significance += '\t{} : p={}, Holm-Bonferroni corrected p={}\n'.format(comparisons[ix],comparison_pvalues[ix],corrected_comparison_pvalues[ix])

with open("sprout-countANOVA.txt", "w") as text_file:
	text_file.seek(0)
	text_file.write(summary.as_text())
	text_file.write('\n\n')
	text_file.write(level_significance)
	text_file.write('\n\n')
	text_file.write(comparison_significance)
	text_file.write('\n\n')
