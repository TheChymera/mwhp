import pandas as pd
import os
from os import path, listdir
import seaborn.apionly as sns
import matplotlib.pyplot as plt
import numpy as np

# Defining style
plt.style.use('style.conf')
custom = ["#4070C0", "#F34444", "#D9C400", "#DD8B20", "#34495e", "#2ecc71"]

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'perfusion.csv'))
df = df.replace('True', '', regex=True)
df = df.replace('False', '', regex=False)
df[['Total Flow Time [s]','Proximal Flow Time [s]','Distal Flow Time [s]','Wound Flow Time [s]']] = df[['Total Flow Time [s]','Proximal Flow Time [s]','Distal Flow Time [s]','Wound Flow Time [s]']].apply(pd.to_numeric)

df_ = df.rename(columns={'Total Flow Time [s]':'Flow Time [s]'})
with plt.style.context('088.conf'):
	fig, ax = plt.subplots()
	ax = sns.swarmplot(
		x='Days Post Transsection',
		y='Flow Time [s]',
		hue='Treatment',
		data=df_,
		hue_order=['Control','AZD4547','Sunitinib','AZD+Sun.'],
		size=7,
		dodge=.15,
		palette=custom,
		)
	ax.set_title('Metric = Total ($|\Delta t_{B-A}|+|\Delta t_{C-B}|+|\Delta t_{D-C}|$)')
	plt.savefig('perfusion_swarmplot.pdf')

df_ = df.melt(id_vars=['subject','Treatment','Days Post Transsection','Perfused Sprouts'],value_vars=['Total Flow Time [s]','Wound Flow Time [s]','Proximal Flow Time [s]','Distal Flow Time [s]'],var_name='Metric',value_name='Flow Time [s]')
df_['Metric'] = df_['Metric'].map({
	'Proximal Flow Time [s]':'Proximal ($\Delta t_{B-A}$)',
	'Wound Flow Time [s]':'Wound ($\Delta t_{C-B}$)',
	'Distal Flow Time [s]':'Distal ($\Delta t_{D-C}$)',
	'Total Flow Time [s]':'Total ($|\Delta t_{B-A}|+|\Delta t_{C-B}|+|\Delta t_{D-C}|$)',
	})
df_ = df_.loc[df_['Metric']!='Total ($|\Delta t_{B-A}|+|\Delta t_{C-B}|+|\Delta t_{D-C}|$)']
with plt.style.context('128.conf'):
	fig, ax = plt.subplots()
	sns.catplot(
		x='Days Post Transsection',
		y='Flow Time [s]',
		hue='Treatment',
		col='Metric',
		data=df_,
		kind='swarm',
		palette=custom,
		hue_order=['Control','AZD4547','Sunitinib','AZD+Sun.'],
		col_order=[
			'Proximal ($\Delta t_{B-A}$)',
			'Wound ($\Delta t_{C-B}$)',
			'Distal ($\Delta t_{D-C}$)',
		#	'Total ($|\Delta t_{B-A}|+|\Delta t_{C-B}|+|\Delta t_{D-C}|$)',
			],
		aspect=1,
		legend_out=False,
		dodge=.15,
		)
	plt.savefig('perfusion_catplot.pdf')

df_ = df.loc[df['Treatment']=='Control']
df_ = df_.melt(id_vars=['subject','Treatment','Days Post Transsection','Perfused Sprouts'],value_vars=['Total Flow Time [s]','Wound Flow Time [s]','Proximal Flow Time [s]','Distal Flow Time [s]'],var_name='Metric',value_name='Flow Time [s]')
df_['Metric'] = df_['Metric'].map({
	'Proximal Flow Time [s]':'Proximal ($\Delta t_{B-A}$)',
	'Wound Flow Time [s]':'Wound ($\Delta t_{C-B}$)',
	'Distal Flow Time [s]':'Distal ($\Delta t_{D-C}$)',
	'Total Flow Time [s]':'Total ($|\Delta t_{B-A}|+|\Delta t_{C-B}|+|\Delta t_{D-C}|$)',
	})
fig, ax = plt.subplots()
sns.catplot(
	x='Days Post Transsection',
	y='Flow Time [s]',
	hue='Treatment',
	col='Metric',
	data=df_,
	kind='swarm',
	palette=custom,
	col_order=[
		'Proximal ($\Delta t_{B-A}$)',
		'Wound ($\Delta t_{C-B}$)',
		'Distal ($\Delta t_{D-C}$)',
		'Total ($|\Delta t_{B-A}|+|\Delta t_{C-B}|+|\Delta t_{D-C}|$)',
		],
	aspect=.7,
	legend_out=False,
	legend=None,
	)
plt.savefig('perfusion_control_catplot.pdf')

