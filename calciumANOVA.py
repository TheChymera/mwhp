# -*- coding: utf8 -*-

import numpy as np
import pandas as pd
from os import path

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'calcium.csv'))
df = df.dropna(axis=0,how='any')

# Modelling
import statsmodels.api as sm
import statsmodels.formula.api as smf
formula = 'Q("Spikes") ~ Artery + Q("Days Post Transsection")'
model = smf.mixedlm(formula, df, groups='subject')
model = smf.ols(formula, df)
fit = model.fit()
summary = fit.summary()
with open("calciumGLM.txt", "w") as text_file:
    text_file.write(summary.as_text())
anova_summary = sm.stats.anova_lm(fit, typ=3)
with open("calciumANOVA.txt", "w") as text_file:
    text_file.write(anova_summary.to_string())

# Wilcoxon

from scipy.stats import wilcoxon
x = df.loc[df['Artery']=='Proximal', 'Spikes'].tolist()
y = df.loc[df['Artery']=='Distal', 'Spikes'].tolist()
result = wilcoxon(x,y)
with open("calciumWILCOXON.txt", "w") as text_file:
    text_file.write(result.__str__())
